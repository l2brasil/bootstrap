var gulp = require('gulp');
var sass = require('gulp-sass');

function styles() {
  return gulp.src([
    'scss/bootstrap.scss',
  ])
  .pipe(sass())
  .pipe(gulp.dest('dist/css'));
}

gulp.task('sass', styles);

function watch() {
  gulp.watch('scss/*.scss', styles);
}

gulp.task('watch', watch);

gulp.task('default', gulp.parallel([
  'sass', 
]));